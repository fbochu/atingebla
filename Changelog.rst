Changelog
=========

0.3 (unreleased)
----------------

- Nothing changed yet.


0.2.1 (2017-08-07)
------------------

- Host source on gitlab.com


0.2 (2017-05-29)
----------------

- Allow to use Gandi OT&E API


0.1.1 (2016-11-05)
------------------

- Fix Python 3.4 list unpacking


0.1 (2016-11-05)
----------------

- First public version
