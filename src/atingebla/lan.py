import logging
import socket
import subprocess

import requests


class LanCheck:
    errors = [
        requests.ConnectionError,
        socket.gaierror,
    ]

    def __init__(self, interface):
        self.interface = interface

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        logger = logging.getLogger(__name__)
        if exc_type in self.errors:
            logger.warning('Catch network exception %s(%s), restart %s', exc_type, exc_val, self.interface)
            subprocess.check_output(['sudo', '-n', 'ifdown', self.interface])
            subprocess.check_output(['sudo', '-n', 'ifup', self.interface])
            return False
