import logging
import logging.config

LEVELS = {
    0: logging.WARNING,
    1: logging.INFO,
    2: logging.DEBUG,
}


def setup(verbosity=0, syslog=False):
    level = LEVELS.get(verbosity, logging.DEBUG)
    handler = 'syslog' if syslog else 'console'

    config = {
        'version': 1,
        'formatters': {
            'syslog': {
                'format': '%(pathname)s[%(process)d]: %(levelname)s: %(message)s',
            },
            'simple': {
                'format': '%(asctime)s: %(message)s',
            },
            'long': {
                'format': '%(asctime)s: %(levelname)s: %(module)s: %(message)s',
            },
        },
        'handlers': {
            'syslog': {
                'class': 'logging.handlers.SysLogHandler',
                'formatter': 'syslog',
                'address': '/dev/log',
            },
            'console': {
                'class': 'logging.StreamHandler',
                'formatter': 'simple' if level > logging.INFO else 'long',
            },
        },
        'loggers': {
            'atingebla': {
                'handlers': [handler],
                'propagate': False,
                'level': level,
            },
        },
        'root': {
            'handlers': [] if verbosity < len(LEVELS) else [handler],
            'level': level,
        }
    }

    logging.config.dictConfig(config)
