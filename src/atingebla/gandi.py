import functools
import logging
import xmlrpc.client

import requests

logger = logging.getLogger(__name__)
API_URL = 'https://rpc.gandi.net/xmlrpc/'
OTE_API_URL = 'https://rpc.ote.gandi.net/xmlrpc/'
IP_URLS = [
    'http://ifconfig.co/json',
    'https://fast-citadel-67310.herokuapp.com/json',  # fallback
]
DEFAULT_TTL = 300


def logged(msg, level=logging.INFO, hidden_args=2):
    def decorator(function):
        @functools.wraps(function)
        def inner(*args):
            try:
                result = function(*args)
            except Exception as e:
                params = args[hidden_args:] + ('{0.__class__.__name__}({0})'.format(e),)
                logger.exception(msg, *params)
                raise
            params = args[hidden_args:] + (result,)
            logger.log(level, msg, *params)
            return result
        return inner
    return decorator


@logged('Get zone id for %s: %s', logging.DEBUG)
def get_zone_id(api, token, domain):
    query = {'name': domain}
    zones = api.domain.zone.list(token, query)
    if not zones:
        raise RuntimeError('Domain %s not found' % domain)

    return zones[0]['id']


@logged('Get IP for in DNS zone #%s for %s: %s')
def get_published_ip(api, token, zone_id, hostname):
    query = {'name': hostname, 'type': 'A'}
    records = api.domain.zone.record.list(token, zone_id, 0, query)
    if not records:
        return None

    return records[0]['value']


@logged('Update DNS record in zone #%s for %s, %s: %s')
def update_record(api, token, zone_id, hostname, ip):
    record = {'name': hostname, 'ttl': DEFAULT_TTL, 'type': 'A', 'value': ip}
    version_id = api.domain.zone.version.new(token, zone_id)

    query = {'name': hostname, 'type': 'A'}
    records = api.domain.zone.record.list(token, zone_id, version_id, query)

    if records:
        query = {'id': records[0]['id']}
        updated = api.domain.zone.record.update(token, zone_id, version_id, query, record)
        if not updated:
            raise RuntimeError('Update DNS record for %s fail' % hostname)
    else:
        added = api.domain.zone.record.add(token, zone_id, version_id, record)
        if not added:
            raise RuntimeError('Add DNS record for %s fail' % hostname)

    return api.domain.zone.version.set(token, zone_id, version_id)


@logged('Get current IP: %s', hidden_args=0)
def get_current_ip():
    for get_ip_url in IP_URLS:
        try:
            response = requests.get(get_ip_url, headers={'User-Agent': 'curl/7.38.0'})
            return response.json()['ip']
        except (requests.RequestException, KeyError):
            continue
    raise RuntimeError('Can not get current IP')


def check_dns(token, domain, hostname, ote=False):
    api_url = OTE_API_URL if ote else API_URL
    api = xmlrpc.client.ServerProxy(api_url)
    zone_id = get_zone_id(api, token, domain)
    published_ip = get_published_ip(api, token, zone_id, hostname)
    current_ip = get_current_ip()

    if published_ip != current_ip:
        return update_record(api, token, zone_id, hostname, current_ip)
