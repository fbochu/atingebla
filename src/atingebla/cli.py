import argparse
import os.path

from . import gandi
from . import lan
from . import log


def stripped_read(filename):
    with open(filename) as fp:
        return fp.read().strip()


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('interface')
    parser.add_argument('domain')
    parser.add_argument('hostname')
    parser.add_argument('--token-file', type=os.path.expanduser, default='~/.config/atingebla.token')
    parser.add_argument('--ote', action='store_true')
    parser.add_argument('--verbose', '-v', action='count', default=0)
    parser.add_argument('--syslog', action='store_true')

    args = parser.parse_args()

    log.setup(args.verbose, args.syslog)
    token = stripped_read(args.token_file)
    with lan.LanCheck(args.interface):
        gandi.check_dns(token, args.domain, args.hostname, ote=args.ote)
