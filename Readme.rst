Atingebla
=========

Small utility to auto-update Gandi DNS record when ip change.

Quick start
-----------

.. code-block:: console

    $ pip install atingebla
    $ atingebla -h
